/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vaayu2k18;

import java.awt.Toolkit;

/***********************************
 * 0 - Yaw
 * 1 - Pitch
 * 2 - Roll
 * 3 - Altitude
 * 4 - Latitude
 * 5 - Longitude
 * 6 - Velocity
 * 7 - Battery
 * 8 - Variometer
 * 9 - Heading
***********************************/

public class GlobalConfig {

    public String allValues, dropValues, orient, alti, xmlFile;

    public int transFrameW, transFrameH;
    public int gaugesFrameX, gaugesFrameY, gaugesFrameW, gaugesFrameH;
    public int altiFrameX, altiFrameY, altiFrameW, altiFrameH;
    public int orientFrameX, orientFrameY, orientFrameW, orientFrameH;

    public int lblFont1, lblFont2, txtFont1, txtFont2, btnFont1, btnFont2;
    public int gr1H, gr1V, gr2H, gr2V, gr3H, gr3V, pnlBorder;

    public double screenW, screenH;

    public GlobalConfig() {
        //Files
        allValues = new String("Files\\All_Values.csv");
        dropValues = new String("Files\\Drop.csv");
        orient = new String("Files\\Orientation.txt");
        alti = new String("Files\\Alti.txt");
        xmlFile = new String("C:\\Users\\Others\\Documents\\NetBeansProjects\\MapTest2__19.2.2017\\web\\books.xml");

        screenW = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        screenH = Toolkit.getDefaultToolkit().getScreenSize().getHeight();

        //Transmission
        transFrameW = (int) (screenW * 0.3645);
        transFrameH = (int) (screenH * 0.5);

        lblFont1 = (int) (transFrameH * 0.060);
        lblFont2 = (int) (transFrameH * 0.0550);
        txtFont1 = (int) (transFrameH * 0.10);
        txtFont2 = (int) (transFrameH * 0.084);
        btnFont1 = (int) (transFrameH * 0.072);
        btnFont2 = (int) (transFrameH * 0.080);

        gr1H = (int) (transFrameH * 0.02);
        gr1V = (int) (transFrameH * 0.04);
        gr2H = (int) (transFrameH * 0.02);
        gr2V = (int) (transFrameH * 0.04);
        gr3H = (int) (transFrameH * 0.02);
        gr3V = (int) (transFrameH * 0.05);
        pnlBorder = (int) (transFrameH * 0.01);

        //Charts
        altiFrameX = 0;
        altiFrameY = (int) (screenH * 0.48);
        altiFrameW = (int) (screenW * 0.353);
        altiFrameH = (int) (screenH * 0.42);

        //Gauges
        gaugesFrameX = (int) (screenW * 0.3545);
        gaugesFrameY = (int) (screenH * 0.4);
        gaugesFrameW = (int) (screenW * 0.29);
        gaugesFrameH = (int) (screenH * 0.55);

        //Orientation
        orientFrameX = 0;
        orientFrameY = (int) (screenH * 0.45);
        orientFrameW = (int) (screenW * 0.3);
        orientFrameH = (int) (screenH * 0.45);
    }
}