/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vaayu2k18;

/**
 *
 * @author Others
 */
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.SimpleDateFormat;
import javafx.embed.swing.JFXPanel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Transmission extends javax.swing.JFrame implements Runnable, SerialPortEventListener, KeyListener, ActionListener {

    /**
     * @param args the command line arguments
     */
    public static final byte DATA_TO_SEND = 'a';
    static CommPortIdentifier portId;
    static Enumeration portList;
    public static final String PORT = "COM4";  //CHECK PORT
    //InputStream inputStream;
    BufferedReader inputStream;
    public static OutputStream outputStream;
    public static SerialPort serialPort;
    Thread readThread;

    private static double velocity, yaw, pitch, roll, alti, lati, longi, aoa, battery, vario, asc_dsc, dropDist, dropTime, heading;
    private static double velocity_sum, yaw_sum = 0, pitch_sum = 0, roll_sum = 0, alti_sum = 0, lati_sum, longi_sum, aoa_sum, battery_sum, vario_sum, asc_dsc_sum, heading_sum;
    private static double VELOCITY_OFFSET, YAW_OFFSET, ROLL_OFFSET, PITCH_OFFSET, ALTI_OFFSET, LATI_OFFSET, LONGI_OFFSET, AOA_OFFSET, VARIO_OFFSET, ASC_DSC_OFFSET;
    private int count = 0, or_count = 10, avg_yaw, avg_pitch, avg_roll;
    static boolean offsetBoolean = false;

    //GUI
    JFrame frame;
    JLabel lblTime, lblVelocity, lblYaw, lblPitch, lblRoll, lblAltitude, lblLatitude, lblLongitude, lblAoa, lblBattery, lblVario, lblAscDsc, lblDropDist, lblDropTime;
    static JTextField txtTime, txtVelocity, txtYaw, txtPitch, txtRoll, txtAltitude, txtLatitude, txtLongitude, txtAoa, txtBattery, txtVario, txtAscDsc, txtDropDist, txtDropTime;
    JButton btnCaliberate, btnCalcDrop, btnDrop, btnRetract;
    BorderLayout brObj;
    GridLayout grObjCenter1, grObjSouth;
    JPanel pnlCenter, pnlEast, pnlSouth, pnlNorth, pnlTime, pnlVelocity, pnlYaw, pnlPitch, pnlRoll, pnlAltitude, pnlLatitude, pnlLongitude, pnlAoa, pnlBattery, pnlVario, pnlAscDsc, pnlDropDist, pnlDropTime;

    //Gauges
    static Gauges g;
    final JFXPanel fxPanelGauge = new JFXPanel();

    //File
    PrintWriter all_values;
    File file2 = new File("Drop Values.txt");
    File file3 = new File("Orientation.txt");
    File file4 = new File("Alti");

    //FileReader in = null;
    FileWriter out = null;
    Date date = new Date();
    Date dropDate;
    boolean calc = false;

    public Transmission(String s) {
        try {
            all_values = new PrintWriter("C:\\Users\\Others\\Desktop\\Vaayu\\All_Values.csv");
            all_values.append("Time of flight" + ',' + "Yaw" + ',' + "Pitch" + ',' + "Roll" + ',' + "Altitude" + ',' + "Latitude" + ',' + "Longitude" + ',' + "AOA" + ',' + "Velocty" + ',' + "Battery" + ',' + "Variometer" + ',' + "Angle of Asc/Desc" + ',' + "Heading" + '\n');

            out = new FileWriter(file2, true);
            out.write("\nTime   Drop Type   Yaw    Roll   Pitch   Altitude  Latitude  Longitude   AOA  Velocity  Battery  Variometer  Angle of Asc/Dsc");
            out.close();

            out = new FileWriter(file3, true);
            out.close();

            out = new FileWriter(file4, true);
            out.close();

            System.out.println("Function: Vaayu2k18 Constructor with String ");
            initJFrameComponents();
            locateTransmissionValues();

            g = new Gauges();
            g.initGauges();
            g.locateGauges(fxPanelGauge);

            Charts.main(new String[0]);
        } catch (IOException ex) {
            Logger.getLogger(Transmission.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Transmission() {
        System.out.println("Function: Vaayu2k18 Constructor  ");
        try {
            serialPort = (SerialPort) portId.open("SimpleReadApp", 2000);
        } catch (PortInUseException e) {
            System.out.println(e);
        }

        try {
            //inputStream = serialPort.getInputStream();
            inputStream = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            serialPort.addEventListener(this);
        } catch (TooManyListenersException e) {
            System.out.println(e);
        }

        serialPort.notifyOnDataAvailable(true);

        try {
            serialPort.setSerialPortParams(9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
        } catch (UnsupportedCommOperationException e) {
            System.out.println(e);
        }

        //KeyEvent code:
        this.setFocusable(true);
        System.out.println(this.isFocusable());
        this.addKeyListener(this); // WOAH!!!!! ?!?!
        readThread = new Thread(this);

        readThread.start();

        try {
            System.out.println("Something..");
            outputStream = Transmission.serialPort.getOutputStream();
        } catch (IOException ex) {
            Logger.getLogger(Transmission.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void run() {
        System.out.println("Function: run with the thread ");
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }

    @Override
    public void serialEvent(SerialPortEvent event) {
        switch (event.getEventType()) {
            case SerialPortEvent.BI:
            case SerialPortEvent.OE:
            case SerialPortEvent.FE:
            case SerialPortEvent.PE:
            case SerialPortEvent.CD:
            case SerialPortEvent.CTS:
            case SerialPortEvent.DSR:
            case SerialPortEvent.RI:
            case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                break;
            case SerialPortEvent.DATA_AVAILABLE:

                try {
                    String inputLine = inputStream.readLine();
                    String arrData[] = inputLine.split("#");
                    System.out.println("\nFunction: serialEvent: ");
                    System.out.print(inputLine);

                    try {
                        yaw = Double.parseDouble(arrData[0]);
                    } catch (NumberFormatException nfe) {
                        yaw = 90.00;
                    }

                    try {
                        pitch = Double.parseDouble(arrData[1]);
                    } catch (NumberFormatException nfe) {
                        pitch = 90.00;
                    }

                    try {
                        roll = Double.parseDouble(arrData[2]);
                    } catch (NumberFormatException nfe) {
                        roll = 90.00;
                    }

                    alti = Double.parseDouble(arrData[3]);
                    lati = Double.parseDouble(arrData[4]);
                    longi = Double.parseDouble(arrData[5]);
                    aoa = Double.parseDouble(arrData[6]);
                    velocity = Double.parseDouble(arrData[7]);
                    battery = Double.parseDouble(arrData[8]);
                    vario = Double.parseDouble(arrData[9]);
                    asc_dsc = Double.parseDouble(arrData[10]);
                    heading = Double.parseDouble(arrData[11]);

                    if (offsetBoolean == false) {
                        txtYaw.setText(Double.toString(yaw));
                        txtPitch.setText(Double.toString(pitch));
                        txtRoll.setText(Double.toString(roll));
                        txtAltitude.setText(Double.toString(alti));
                        txtLatitude.setText(Double.toString(lati));
                        txtLongitude.setText(Double.toString(longi));
                        txtAoa.setText(Double.toString(aoa));
                        txtVelocity.setText(Double.toString(velocity));
                        txtBattery.setText(Double.toString(battery));
                        txtVario.setText(Double.toString(vario));
                        txtAscDsc.setText(Double.toString(asc_dsc));

                        g.setGauge(velocity, battery, aoa, heading, asc_dsc);
                    } else {
                        count++;

                        if (count <= 10) {
                            YAW_OFFSET += yaw;
                            ROLL_OFFSET += roll;
                            PITCH_OFFSET += pitch;
                            ALTI_OFFSET += alti;
                            AOA_OFFSET += aoa;
                            VELOCITY_OFFSET += velocity;
                            VARIO_OFFSET += vario;
                            ASC_DSC_OFFSET += asc_dsc;

                            txtYaw.setText(Double.toString(yaw));
                            txtPitch.setText(Double.toString(pitch));
                            txtRoll.setText(Double.toString(roll));
                            txtAltitude.setText(Double.toString(alti));
                            txtLatitude.setText(Double.toString(lati));
                            txtLongitude.setText(Double.toString(longi));
                            txtAoa.setText(Double.toString(aoa));
                            txtVelocity.setText(Double.toString(velocity));
                            txtBattery.setText(Double.toString(battery));
                            txtVario.setText(Double.toString(vario));
                            txtAscDsc.setText(Double.toString(asc_dsc));

                            g.setGauge(velocity, battery, aoa, heading, asc_dsc);
                        } else {
                            if (count == 11) {
                                YAW_OFFSET /= 10;
                                ROLL_OFFSET /= 10;
                                PITCH_OFFSET /= 10;
                                ALTI_OFFSET /= 10;
                                AOA_OFFSET /= 10;
                                VELOCITY_OFFSET /= 10;
                                VARIO_OFFSET /= 10;
                                ASC_DSC_OFFSET /= 10;

                                date = new Date();
                                System.out.println("Date: " + date);
                            }

                            if (calc) {
                                Date now = new Date();
                                double diff = dropDate.getTime() - now.getTime();
                                txtDropTime.setText(Double.toString(diff));
                            }

                            txtYaw.setText(Double.toString(yaw - YAW_OFFSET));
                            txtPitch.setText(Double.toString(pitch - PITCH_OFFSET));
                            txtRoll.setText(Double.toString(roll - ROLL_OFFSET));
                            txtAltitude.setText(Double.toString(alti - ALTI_OFFSET));
                            txtLatitude.setText(Double.toString(lati));
                            txtLongitude.setText(Double.toString(longi));
                            txtAoa.setText(Double.toString(aoa - AOA_OFFSET));
                            txtVelocity.setText(Double.toString(velocity - VELOCITY_OFFSET));
                            txtBattery.setText(Double.toString(battery));
                            txtVario.setText(Double.toString(vario - VARIO_OFFSET));
                            txtAscDsc.setText(Double.toString(asc_dsc - ASC_DSC_OFFSET));

                            g.setGauge(velocity - VELOCITY_OFFSET, battery, aoa - AOA_OFFSET, heading, asc_dsc - ASC_DSC_OFFSET);

                            Date newdate = new Date();
                            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

                            System.out.println("NDate: " + newdate);

                            long diff = newdate.getTime() - date.getTime();

                            long diffMSeconds = diff % 1000;
                            long diffSeconds = diff / 1000 % 60;
                            long diffMinutes = diff / (60 * 1000) % 60;
                            long diffHours = diff / (60 * 60 * 1000) % 24;

                            String strDate = diffHours + ":" + diffMinutes + ":" + diffSeconds + ":" + diffMSeconds;

                            txtTime.setText(strDate);

                            all_values.append(strDate + ',' + Double.toString(yaw - YAW_OFFSET) + ',' + Double.toString(pitch - PITCH_OFFSET) + ',' + Double.toString(roll - ROLL_OFFSET) + ',' + Double.toString(alti - ALTI_OFFSET) + ',');
                            all_values.append(Double.toString(lati) + ',' + Double.toString(longi) + ',' + Double.toString(aoa - AOA_OFFSET) + ',' + Double.toString(velocity) + ',');
                            all_values.append(Double.toString(battery) + ',' + Double.toString(vario - VARIO_OFFSET) + ',' + Double.toString(asc_dsc - ASC_DSC_OFFSET) + ',' + Double.toString(heading) + '\n');
                            /*if(or_count==10) {
                                //Find avg
                                avg_yaw /= 10;
                                avg_pitch /=10;
                                avg_roll /=10;
                                        
                                //Check if file is empty
                                        
                            
                                //Write to file
                                out = new FileWriter(file3,true);
                                out.append(System.lineSeparator());
                                out.write(avg_yaw+" "+avg_pitch+" "+avg_roll);
                                out.close();
                                        
                                //Reset
                                avg_yaw = 0;
                                avg_pitch = 0;
                                avg_roll = 0;
                                or_count = 0;
                            }
                            
                            avg_yaw += yaw - YAW_OFFSET;
                            avg_pitch += pitch - PITCH_OFFSET;
                            avg_roll += roll - ROLL_OFFSET;
                            or_count++;*/

                            out = new FileWriter(file4, true);
                            out.append(System.lineSeparator());
                            out.write((alti - ALTI_OFFSET) + " ");
                            out.close();
                        }
                    }
                } catch (IOException e) {
                    System.out.println("\n\n" + e + "\n");
                }

                break;
        }
    }

    public static class SerialWriter implements Runnable {

        OutputStream out;
        char character;

        public SerialWriter(OutputStream out, char ch) {
            this.out = out;
            character = ch;
        }

        public void run() {
            try {
                this.out.write(character);
            } catch (IOException ex) {
                Logger.getLogger(Transmission.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(btnCaliberate)) {
            System.out.println("\n\nCaliberate");
            if (offsetBoolean == false) //SET OFFSET VALUES
            {
                System.out.println("Caliberate In");
                offsetBoolean = true;
            }
            System.out.println();
        } else if (e.getSource().equals(btnCalcDrop)) {
            calc = true;

            double u = velocity - VELOCITY_OFFSET;
            double h = alti - ALTI_OFFSET;

            double dFrom60 = 30.5 + Math.tan(60);
            double dropDist = u * Math.sqrt(h / 4.9);
            double centreDist = dFrom60 + 18.3;
            double remainingDist = centreDist - dropDist;

            txtDropDist.setText(Double.toString(dropDist));

            //Get current time
            Calendar calendar = Calendar.getInstance();
            Date currTime = calendar.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss S");
            //System.out.println("Current Time: " + sdf.format(currTime));

            //Find remaining time
            int remainingTime = (int) ((remainingDist / u) * 1000);

            System.out.println(remainingDist + " " + u);

            //Find drop time
            calendar.add(Calendar.MILLISECOND, remainingTime);
            dropDate = calendar.getTime();
            //System.out.println("Time after "+ remainingTime +" milliseconds: " + sdf.format(dropTime));

        } else if (e.getSource().equals(btnDrop) && date != null) {
            try {
                (new Thread(new SerialWriter(outputStream, '1'))).start();
                System.out.println("\n\nDrop!!\n");

                Date newdate = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

                long diff = newdate.getTime() - date.getTime();

                long diffSeconds = diff / 1000 % 60;
                long diffMinutes = diff / (60 * 1000) % 60;
                long diffHours = diff / (60 * 60 * 1000) % 24;

                String strDate = diffHours + ":" + diffMinutes + ":" + diffSeconds;

                out = new FileWriter(file2, true);
                out.append(System.lineSeparator());
                out.write(strDate + "\tDrop  \t" + (yaw - YAW_OFFSET) + "\t" + (pitch - PITCH_OFFSET) + "\t" + (roll - ROLL_OFFSET) + "\t" + (alti - ALTI_OFFSET) + "\t" + (lati - LATI_OFFSET) + "\t" + (longi - LONGI_OFFSET) + "  \t" + (aoa - AOA_OFFSET) + "\t" + (velocity - VELOCITY_OFFSET) + "\t" + battery + "\t" + (vario - VARIO_OFFSET) + "\t" + (asc_dsc - ASC_DSC_OFFSET));
                out.close();
            } catch (IOException ex) {
                Logger.getLogger(Transmission.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (e.getSource().equals(btnRetract) && date != null) {
            (new Thread(new SerialWriter(outputStream, '2'))).start();
            System.out.println("\n\nRetract!!\n");
        }
    }

    void initJFrameComponents() {
        frame = new JFrame("GUI!!");

        lblTime = new JLabel("  Time  ");
        lblYaw = new JLabel("    Yaw    ");
        lblPitch = new JLabel("    Pitch    ");
        lblRoll = new JLabel("    Roll    ");
        lblAltitude = new JLabel("Altitude");
        lblLongitude = new JLabel("Latitude");
        lblLatitude = new JLabel("Longitude");
        lblAoa = new JLabel("    AOA    ");
        lblVelocity = new JLabel("Velocity");
        lblBattery = new JLabel("Battery");
        lblVario = new JLabel("Variometer");
        lblAscDsc = new JLabel("Asc/Dsc");
        lblDropDist = new JLabel("Drop Distance:");
        lblDropTime = new JLabel("Drop Time:");

        txtTime = new JTextField(6);
        txtYaw = new JTextField(6);
        txtPitch = new JTextField(6);
        txtRoll = new JTextField(6);
        txtAltitude = new JTextField(6);
        txtLongitude = new JTextField(6);
        txtLatitude = new JTextField(6);
        txtAoa = new JTextField(6);
        txtVelocity = new JTextField(6);
        txtBattery = new JTextField(6);
        txtVario = new JTextField(6);
        txtAscDsc = new JTextField(6);
        txtDropDist = new JTextField(6);
        txtDropTime = new JTextField(6);

        btnCaliberate = new JButton("Caliberate");
        btnCalcDrop = new JButton("Calculate");
        btnDrop = new JButton("Drop");
        btnRetract = new JButton("Retract");

        lblTime.setFont(new java.awt.Font("Tahoma", 1, 24));
        lblYaw.setFont(new java.awt.Font("Tahoma", 1, 24));
        lblPitch.setFont(new java.awt.Font("Tahoma", 1, 24));
        lblRoll.setFont(new java.awt.Font("Tahoma", 1, 24));
        lblAltitude.setFont(new java.awt.Font("Tahoma", 1, 24));
        lblLatitude.setFont(new java.awt.Font("Tahoma", 1, 24));
        lblLongitude.setFont(new java.awt.Font("Tahoma", 1, 24));
        lblAoa.setFont(new java.awt.Font("Tahoma", 1, 24));
        lblVelocity.setFont(new java.awt.Font("Tahoma", 1, 24));
        lblBattery.setFont(new java.awt.Font("Tahoma", 1, 24));
        lblVario.setFont(new java.awt.Font("Tahoma", 1, 24));
        lblAscDsc.setFont(new java.awt.Font("Tahoma", 1, 24));
        lblDropDist.setFont(new java.awt.Font("Tahoma", 1, 24));
        lblDropTime.setFont(new java.awt.Font("Tahoma", 1, 24));

        txtTime.setHorizontalAlignment(JTextField.CENTER);
        txtTime.setFont(new java.awt.Font("Times New Roman", 1, 32));
        txtTime.setEditable(false);
        txtYaw.setHorizontalAlignment(JTextField.CENTER);
        txtYaw.setFont(new java.awt.Font("Times New Roman", 1, 32));
        txtYaw.setEditable(false);
        txtPitch.setHorizontalAlignment(JTextField.CENTER);
        txtPitch.setFont(new java.awt.Font("Times New Roman", 1, 32));
        txtPitch.setEditable(false);
        txtRoll.setHorizontalAlignment(JTextField.CENTER);
        txtRoll.setFont(new java.awt.Font("Times New Roman", 1, 32));
        txtRoll.setEditable(false);
        txtAltitude.setHorizontalAlignment(JTextField.CENTER);
        txtAltitude.setFont(new java.awt.Font("Times New Roman", 1, 32));
        txtAltitude.setEditable(false);
        txtLatitude.setHorizontalAlignment(JTextField.CENTER);
        txtLatitude.setFont(new java.awt.Font("Times New Roman", 1, 32));
        txtLatitude.setEditable(false);
        txtLongitude.setHorizontalAlignment(JTextField.CENTER);
        txtLongitude.setFont(new java.awt.Font("Times New Roman", 1, 32));
        txtLongitude.setEditable(false);
        txtAoa.setHorizontalAlignment(JTextField.CENTER);
        txtAoa.setFont(new java.awt.Font("Times New Roman", 1, 32));
        txtAoa.setEditable(false);
        txtVelocity.setHorizontalAlignment(JTextField.CENTER);
        txtVelocity.setFont(new java.awt.Font("Times New Roman", 1, 32));
        txtVelocity.setEditable(false);
        txtBattery.setHorizontalAlignment(JTextField.CENTER);
        txtBattery.setFont(new java.awt.Font("Times New Roman", 1, 32));
        txtBattery.setEditable(false);
        txtVario.setHorizontalAlignment(JTextField.CENTER);
        txtVario.setFont(new java.awt.Font("Times New Roman", 1, 32));
        txtVario.setEditable(false);
        txtAscDsc.setHorizontalAlignment(JTextField.CENTER);
        txtAscDsc.setFont(new java.awt.Font("Times New Roman", 1, 32));
        txtAscDsc.setEditable(false);
        txtDropDist.setHorizontalAlignment(JTextField.CENTER);
        txtDropDist.setFont(new java.awt.Font("Times New Roman", 1, 32));
        txtDropDist.setEditable(false);
        txtDropTime.setHorizontalAlignment(JTextField.CENTER);
        txtDropTime.setFont(new java.awt.Font("Times New Roman", 1, 32));
        txtDropTime.setEditable(false);

        btnCaliberate.setFont(new java.awt.Font("Tahoma", 1, 24));
        btnCalcDrop.setFont(new java.awt.Font("Tahoma", 1, 24));
        btnDrop.setFont(new java.awt.Font("Tahoma", 1, 24));
        btnRetract.setFont(new java.awt.Font("Tahoma", 1, 24));

        brObj = new BorderLayout();
        grObjCenter1 = new GridLayout(5, 4, 10, 10);
        grObjSouth = new GridLayout(2, 2, 3, 2);
        pnlCenter = new JPanel();
        pnlEast = new JPanel();
        pnlSouth = new JPanel();
        pnlNorth = new JPanel();
        pnlTime = new JPanel();
        pnlYaw = new JPanel();
        pnlPitch = new JPanel();
        pnlRoll = new JPanel();
        pnlAltitude = new JPanel();
        pnlLongitude = new JPanel();
        pnlLatitude = new JPanel();
        pnlAoa = new JPanel();
        pnlVelocity = new JPanel();
        pnlBattery = new JPanel();
        pnlVario = new JPanel();
        pnlAscDsc = new JPanel();
        pnlDropDist = new JPanel();
        pnlDropTime = new JPanel();
    }

    void locateTransmissionValues() {
        frame.setLayout(brObj);

        frame.add(pnlCenter, BorderLayout.CENTER);

        pnlCenter.setLayout(grObjCenter1);

        pnlCenter.add(pnlTime);             //1,1
        pnlTime.add(lblTime);
        pnlTime.add(txtTime);
        pnlCenter.add(pnlYaw);              //1,2
        pnlYaw.add(lblYaw);
        pnlYaw.add(txtYaw);
        pnlCenter.add(pnlPitch);            //1,3
        pnlPitch.add(lblPitch);
        pnlPitch.add(txtPitch);
        pnlCenter.add(pnlRoll);             //1,4
        pnlRoll.add(lblRoll);
        pnlRoll.add(txtRoll);
        pnlCenter.add(pnlAltitude);         //2,1
        pnlAltitude.add(lblAltitude);
        pnlAltitude.add(txtAltitude);
        pnlCenter.add(pnlLatitude);         //2,2
        pnlLatitude.add(lblLatitude);
        pnlLatitude.add(txtLatitude);
        pnlCenter.add(pnlLongitude);        //2,3
        pnlLongitude.add(lblLongitude);
        pnlLongitude.add(txtLongitude);
        pnlCenter.add(pnlAoa);              //2,4
        pnlAoa.add(lblAoa);
        pnlAoa.add(txtAoa);
        pnlCenter.add(pnlVelocity);         //3,1
        pnlVelocity.add(lblVelocity);
        pnlVelocity.add(txtVelocity);
        pnlCenter.add(pnlBattery);          //3,2
        pnlBattery.add(lblBattery);
        pnlBattery.add(txtBattery);
        pnlCenter.add(pnlVario);            //3,3
        pnlVario.add(lblVario);
        pnlVario.add(txtVario);
        pnlCenter.add(pnlAscDsc);           //3,4
        pnlAscDsc.add(lblAscDsc);
        pnlAscDsc.add(txtAscDsc);

        pnlCenter.add(btnCaliberate);       //4,1
        pnlCenter.add(btnCalcDrop);         //4,2
        pnlCenter.add(btnDrop);             //4,3
        pnlCenter.add(btnRetract);          //4,4

        btnCaliberate.addActionListener(this);
        btnCalcDrop.addActionListener(this);
        btnDrop.addActionListener(this);
        btnRetract.addActionListener(this);

        pnlCenter.add(lblDropDist);
        pnlCenter.add(txtDropDist);
        pnlCenter.add(lblDropTime);
        pnlCenter.add(txtDropTime);

        frame.add(pnlSouth, BorderLayout.SOUTH);
        pnlSouth.add(fxPanelGauge);

        frame.setSize(700, 1000);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // TODO code application logic here
        Transmission t = new Transmission("s");

        portList = CommPortIdentifier.getPortIdentifiers();
        System.out.println("here/" + portList.hasMoreElements());

        while (portList.hasMoreElements()) {

            portId = (CommPortIdentifier) portList.nextElement();
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                if (portId.getName().equals(PORT)) {
                    System.out.println("calling constructor...");
                    Transmission reader = new Transmission();
                }
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            //send signal to drop the package

            try {
                // TODO add your handling code here:
                outputStream = Transmission.serialPort.getOutputStream();
            } catch (IOException ex) {
                Logger.getLogger(Transmission.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
