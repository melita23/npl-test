/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vaayu2k18;

/**
 *
 * @author Others
 */
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.SimpleDateFormat;
import javafx.embed.swing.JFXPanel;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Trans2 extends javax.swing.JFrame implements Runnable, SerialPortEventListener, KeyListener, ActionListener {

    /**
     * @param args the command line arguments
     */
    static CommPortIdentifier portId;
    static Enumeration portList;
    public static final String PORT = "COM4";  //CHECK PORT
    BufferedReader inputStream;
    public static OutputStream outputStream;
    public static SerialPort serialPort;
    Thread readThread;

    private static String[] split = new String[2]; 
    private static double[] curr = new double[10]; 
    private static double[] sum = new double[10];
    private static double[] offset = new double[10];
    
    //GUI
    JFrame frame;
    JLabel lblTime, lblAltitude, lblDropDist, lblDropTime, lblDropAlti;
    static JTextField txtTime, txtAltitude, txtDropDist, txtDropTime, txtDropAlti;
    JButton btnCaliberate, btnStart, btnCalcDrop, btnDrop2, btnDrop4, btnRetract;
    BorderLayout brObj;
    GridLayout grObjCenter1, grObjCenter2, grObjCenter3;
    JPanel pnlCenter, pnl1, pnl2, pnl3, pnlTime, pnlAltitude, pnlDropDist, pnlDropTime, pnlDropAlti;

    //Gauges
    static SteelGauge g;
    
    //Charts
    static Charts c;
    
    //Maps
    static Maps map;
    
    //File
    static File all1, drop1, orient1, altitude1;

    //FileReader in = null;
    static FileWriter all, drop, orient, altitude;
    Date date = new Date();
    Date dropDate;
    static boolean calc = false, start = false;
    static GlobalConfig gc;
    static int index;
    private int count = 0, or_count = 0, avg_yaw, avg_pitch, avg_roll;
    static boolean offsetBoolean = false;
    static String strNewDate;

    public Trans2(String s) {
        
        gc = new GlobalConfig();
        
        all1 = new File(gc.allValues);
        drop1 = new File(gc.dropValues);
        orient1 = new File(gc.orient);
        altitude1 = new File(gc.alti);

        try {
            all = new FileWriter(all1, false);
            all.append("Time Stamp" + ',' + "Time of flight" + ',' + "Yaw" + ',' + "Pitch" + ',' + "Roll" + ',' + "Altitude" + ',' + "Latitude" + ',' + "Longitude" + ',' + "Velocty" + ',' + "Battery" + ',' + "Variometer" + ',' + "Heading" + '\n');
            all.flush();

            drop = new FileWriter(drop1, false);
            drop.append("Time Stamp" + ',' + "Time of flight" + ',' + "No of payloads" + ',' + "Yaw" + ',' + "Pitch" + ',' + "Roll" + ',' + "Altitude" + ',' + "Latitude" + ',' + "Longitude" + ',' + "Velocty" + ',' + "Battery" + ',' + "Variometer" + ',' + "Heading" + '\n');
            drop.flush();

            orient = new FileWriter(orient1, false);
            orient.append("");
            orient.flush();

            altitude = new FileWriter(altitude1, false);
            altitude.append("");
            altitude.flush();

            System.out.println("Function: Vaayu2k18 Constructor with String ");
            initJFrameComponents();
            locateTransmissionValues();

            g = new SteelGauge(gc);
            g.createAndShowUI();

            Charts.main(new String[0]);
            
            map = new Maps(gc.xmlFile);

        } catch (IOException ex) {
            Logger.getLogger(Trans2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Trans2() {
        System.out.println("Function: Vaayu2k18 Constructor  ");
        try {
            serialPort = (SerialPort) portId.open("SimpleReadApp", 2000);
        } catch (PortInUseException e) {
            System.out.println(e);
        }

        try {
            inputStream = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            serialPort.addEventListener(this);
        } catch (TooManyListenersException e) {
            System.out.println(e);
        }

        serialPort.notifyOnDataAvailable(true);

        try {
            serialPort.setSerialPortParams(9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
        } catch (UnsupportedCommOperationException e) {
            System.out.println(e);
        }

        //KeyEvent code:
        this.setFocusable(true);
        System.out.println(this.isFocusable());
        this.addKeyListener(this);
        readThread = new Thread(this);

        readThread.start();

        try {
            System.out.println("Something..");
            outputStream = Trans2.serialPort.getOutputStream();
        } catch (IOException ex) {
            Logger.getLogger(Trans2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void run() {
        System.out.println("Function: run with the thread ");
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }

    @Override
    public void serialEvent(SerialPortEvent event) {
        switch (event.getEventType()) {
            case SerialPortEvent.BI:
            case SerialPortEvent.OE:
            case SerialPortEvent.FE:
            case SerialPortEvent.PE:
            case SerialPortEvent.CD:
            case SerialPortEvent.CTS:
            case SerialPortEvent.DSR:
            case SerialPortEvent.RI:
            case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                break;
            case SerialPortEvent.DATA_AVAILABLE:

                try {
                    String inputLine = inputStream.readLine();
                    String arrData[] = inputLine.split("#");
                    //System.out.println("\nFunction: serialEvent: ");
                    System.out.println(inputLine);
                    
                    for(int i=0;i<10;i++) {
                        try {
                            split = arrData[i].split(":");
                            index = Integer.parseInt(split[0]);
                            curr[index] = Double.parseDouble(split[1]);
                        } catch (NumberFormatException nfe) {
                            if(index<3 && split[1].equals("nan"))
                                curr[index] = 90.00;
                        }
                    }

                    if (offsetBoolean == false) {
                        txtAltitude.setText(Double.toString(curr[3])+" ");

                        g.setGauges(curr[6], curr[7], curr[8], curr[9]);
                        map.setMap(curr[4], curr[5]);
                    } else {
                        count++;

                        if (count <= 10) {
                            for(int i=0;i<10;i++) {
                                if(i!=4 && i!=5 && i!=7 && i!=9)
                                    offset[i] += curr[i]; 
                            }

                            txtAltitude.setText(Double.toString(curr[3])+" ");

                            g.setGauges(curr[6], curr[7], curr[8], curr[9]);
                            map.setMap(curr[4], curr[5]);
                        } else {
                            if (count == 11) {
                                for(int i=0;i<10;i++) {
                                    if(i!=4 && i!=5 && i!=7 && i!=9)
                                        offset[i] /= 10; 
                                }
                            }

                            if (calc) {
                                Date now = new Date();
                                double diff = dropDate.getTime() - now.getTime();
                                txtDropTime.setText(Double.toString(diff));
                            }

                            Date newdate = new Date();
                            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                            strNewDate = dateFormat.format(newdate.getTime());

                            txtAltitude.setText(Double.toString(curr[3]-offset[3])+" ");
                            g.setGauges(curr[6]-offset[6], curr[7], curr[8]-offset[8], curr[9]);
                            map.setMap(curr[4], curr[5]);

                            if (start) {
                                long diff = newdate.getTime() - date.getTime();

                                //long diffMSeconds = diff % 1000;
                                long diffSeconds = diff / 1000 % 60;
                                long diffMinutes = diff / (60 * 1000) % 60;
                                long diffHours = diff / (60 * 60 * 1000) % 24;

                                String strDate = diffHours + ":" + diffMinutes + ":" + diffSeconds;

                                txtTime.setText(strDate);

                                all.append(strNewDate + ',' + strDate + ',' + Double.toString(curr[0]-offset[0]) + ',' + Double.toString(curr[1]-offset[1]) + ',' + Double.toString(curr[2]-offset[2]) + ',' + Double.toString(curr[3]-offset[3]) + ',' + Double.toString(curr[4]) + ',' + Double.toString(curr[5]) + ',' + Double.toString(curr[6]-offset[6]) + ',' + Double.toString(curr[7]) + ',' + Double.toString(curr[8]-offset[8]) + ',' + Double.toString(curr[9]) + '\n');
                                all.flush();

                                if(or_count==10) {
                                    //Find avg
                                    avg_yaw /= 10;
                                    avg_pitch /=10;
                                    avg_roll /=10;

                                    //Write to file
                                    orient = new FileWriter(orient1, false);
                                    orient.write(Double.toString(curr[0]-offset[0])+" "+Double.toString(curr[1]-offset[1])+" "+Double.toString(curr[2]-offset[2]));
                                    orient.close();

                                    //Reset
                                    avg_yaw = 0;
                                    avg_pitch = 0;
                                    avg_roll = 0;
                                    or_count = 0;
                                }

                                avg_yaw += curr[0]-offset[0];
                                avg_pitch += curr[1]-offset[1];
                                avg_roll += curr[2]-offset[2];
                                or_count++;
                                
                                altitude.append(Double.toString(curr[3]-offset[3])+" ");
                                altitude.flush();
                            }
                        }
                    }
                } catch (IOException e) {
                    System.out.println("\n\n" + e + "\n");
                }

                break;
        }
    }

    public static class SerialWriter implements Runnable {

        OutputStream out;
        char character;

        public SerialWriter(OutputStream out, char ch) {
            this.out = out;
            character = ch;
        }

        public void run() {
            try {
                this.out.write(character);
            } catch (IOException ex) {
                Logger.getLogger(Trans2.class.getName()).log(Level.SEVERE, null, ex);
            } catch(NullPointerException npe) {
                Logger.getLogger(Trans2.class.getName()).log(Level.SEVERE, null, npe);
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(btnCaliberate)) {
            System.out.println("\n\nCaliberate");
            if (offsetBoolean == false) //SET OFFSET VALUES
            {
                System.out.println("Caliberate In");
                offsetBoolean = true;
                btnCaliberate.disable();
            }
            System.out.println();
        } else if (e.getSource().equals(btnStart)) {
            if (!start) {
                System.out.println("Start In");
                start = true;

                date = new Date();
                System.out.println("Date: " + date);
            }

        } else if (e.getSource().equals(btnCalcDrop)) {
            calc = true;

            double u = curr[6]-offset[6];
            double h = curr[3]-offset[3];

            double dFrom60 = 30.5 + Math.tan(60);
            double dropDist = u * Math.sqrt(h / 4.9);
            double centreDist = dFrom60 + 18.3;
            double remainingDist = centreDist - dropDist;

            txtDropDist.setText(Double.toString(dropDist));

            //Get current time
            Calendar calendar = Calendar.getInstance();
            Date currTime = calendar.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss S");
            //System.out.println("Current Time: " + sdf.format(currTime));

            //Find remaining time
            int remainingTime = (int) ((remainingDist / u) * 1000);

            System.out.println(remainingDist + " " + u);

            //Find drop time
            calendar.add(Calendar.MILLISECOND, remainingTime);
            dropDate = calendar.getTime();
            //System.out.println("Time after "+ remainingTime +" milliseconds: " + sdf.format(dropTime));

        } else if (e.getSource().equals(btnDrop2) && date != null) {
            try {
                (new Thread(new SerialWriter(outputStream, '2'))).start();
                System.out.println("\n\nDrop!!\n");

                Date newdate = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

                long diff = newdate.getTime() - date.getTime();

                long diffSeconds = diff / 1000 % 60;
                long diffMinutes = diff / (60 * 1000) % 60;
                long diffHours = diff / (60 * 60 * 1000) % 24;

                String strDate = diffHours + ":" + diffMinutes + ":" + diffSeconds;

                txtDropAlti.setText(Double.toString(curr[3]-offset[3]));
                drop.append(strNewDate + ',' + strDate + ',' + "2" + ',' + Double.toString(curr[0]-offset[0]) + ',' + Double.toString(curr[1]-offset[1]) + ',' + Double.toString(curr[2]-offset[2]) + ',' + Double.toString(curr[3]-offset[3]) + ',' + Double.toString(curr[4]) + ',' + Double.toString(curr[5]) + ',' + Double.toString(curr[6]-offset[6]) + ',' + Double.toString(curr[7]) + ',' + Double.toString(curr[8]-offset[8]) + ',' + Double.toString(curr[9]) + '\n');
                drop.flush();
            } catch (IOException ex) {
                Logger.getLogger(Trans2.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (e.getSource().equals(btnDrop4) && date != null) {
            try {
                (new Thread(new SerialWriter(outputStream, '4'))).start();
                System.out.println("\n\nDrop!!\n");

                Date newdate = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

                long diff = newdate.getTime() - date.getTime();

                long diffSeconds = diff / 1000 % 60;
                long diffMinutes = diff / (60 * 1000) % 60;
                long diffHours = diff / (60 * 60 * 1000) % 24;

                String strDate = diffHours + ":" + diffMinutes + ":" + diffSeconds;

                txtDropAlti.setText(Double.toString(curr[3]-offset[3]));
                drop.append(strNewDate + ',' + strDate + ',' + "4" + ',' + Double.toString(curr[0]-offset[0]) + ',' + Double.toString(curr[1]-offset[1]) + ',' + Double.toString(curr[2]-offset[2]) + ',' + Double.toString(curr[3]-offset[3]) + ',' + Double.toString(curr[4]) + ',' + Double.toString(curr[5]) + ',' + Double.toString(curr[6]-offset[6]) + ',' + Double.toString(curr[7]) + ',' + Double.toString(curr[8]-offset[8]) + ',' + Double.toString(curr[9]) + '\n');
                drop.flush();
            } catch (IOException ex) {
                Logger.getLogger(Trans2.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else if (e.getSource().equals(btnRetract) && date != null) {
            (new Thread(new SerialWriter(outputStream, '1'))).start();
            System.out.println("\n\nRetract!!\n");
        }
    }

    void initJFrameComponents() {
        frame = new JFrame("GUI!!");

        lblTime = new JLabel("Flight Time");
        lblAltitude = new JLabel("Altitude");
        lblDropDist = new JLabel("Drop Distance");
        lblDropTime = new JLabel("Drop Time");
        lblDropAlti = new JLabel("Drop Altitude");

        txtTime = new JTextField(6);
        txtAltitude = new JTextField(6);
        txtDropDist = new JTextField(6);
        txtDropTime = new JTextField(6);
        txtDropAlti = new JTextField(6);

        btnCaliberate = new JButton("Caliberate");
        btnStart = new JButton("Start");
        btnCalcDrop = new JButton("Calculate");
        btnDrop2 = new JButton("Drop 2");
        btnDrop4 = new JButton("Drop 4");
        btnRetract = new JButton("Retract");

        lblTime.setFont(new java.awt.Font("Tahoma", 1, gc.lblFont1));
        lblTime.setHorizontalAlignment(JTextField.CENTER);
        lblAltitude.setFont(new java.awt.Font("Tahoma", 1, gc.lblFont1));
        lblAltitude.setHorizontalAlignment(JTextField.CENTER);
        lblDropDist.setFont(new java.awt.Font("Tahoma", 1, gc.lblFont2));
        lblDropDist.setHorizontalAlignment(JTextField.CENTER);
        lblDropTime.setFont(new java.awt.Font("Tahoma", 1, gc.lblFont2));
        lblDropTime.setHorizontalAlignment(JTextField.CENTER);
        lblDropAlti.setFont(new java.awt.Font("Tahoma", 1, gc.lblFont2));
        lblDropAlti.setHorizontalAlignment(JTextField.CENTER);

        txtTime.setHorizontalAlignment(JTextField.CENTER);
        txtTime.setFont(new java.awt.Font("Times New Roman", 1, gc.txtFont1));
        txtTime.setEditable(false);
        txtAltitude.setHorizontalAlignment(JTextField.CENTER);
        txtAltitude.setFont(new java.awt.Font("Times New Roman", 1, gc.txtFont1));
        txtAltitude.setEditable(false);
        txtDropDist.setHorizontalAlignment(JTextField.CENTER);
        txtDropDist.setFont(new java.awt.Font("Times New Roman", 1, gc.txtFont2));
        txtDropDist.setEditable(false);
        txtDropTime.setHorizontalAlignment(JTextField.CENTER);
        txtDropTime.setFont(new java.awt.Font("Times New Roman", 1, gc.txtFont2));
        txtDropTime.setEditable(false);
        txtDropAlti.setHorizontalAlignment(JTextField.CENTER);
        txtDropAlti.setFont(new java.awt.Font("Times New Roman", 1, gc.txtFont2));
        txtDropAlti.setEditable(false);

        btnCaliberate.setFont(new java.awt.Font("Tahoma", 1, gc.btnFont1));
        btnStart.setFont(new java.awt.Font("Tahoma", 1, gc.btnFont2));
        btnCalcDrop.setFont(new java.awt.Font("Tahoma", 1, gc.btnFont1));
        btnDrop2.setFont(new java.awt.Font("Tahoma", 1, gc.btnFont2));
        btnDrop4.setFont(new java.awt.Font("Tahoma", 1, gc.btnFont2));
        btnRetract.setFont(new java.awt.Font("Tahoma", 1, gc.btnFont2));

        brObj = new BorderLayout();
        grObjCenter1 = new GridLayout(2, 2, gc.gr1H, gc.gr1V);
        grObjCenter2 = new GridLayout(2, 3, gc.gr2H, gc.gr2V);
        grObjCenter3 = new GridLayout(2, 3, gc.gr3H, gc.gr3V);
        pnlCenter = new JPanel();
        pnl1 = new JPanel();
        pnl2 = new JPanel();
        pnl3 = new JPanel();
        
        pnl3.setBorder(BorderFactory.createEmptyBorder(gc.pnlBorder,0,gc.pnlBorder,0));
    }

    void locateTransmissionValues() {
        frame.setLayout(brObj);

        frame.add(pnlCenter, BorderLayout.CENTER);

        pnlCenter.add(pnl1);
        pnl1.setLayout(grObjCenter1);

        pnl1.add(lblTime);
        pnl1.add(lblAltitude);
        pnl1.add(txtTime);
        pnl1.add(txtAltitude);

        pnlCenter.add(pnl2);
        pnl2.setLayout(grObjCenter2);

        pnl2.add(lblDropDist);
        pnl2.add(lblDropTime);
        pnl2.add(lblDropAlti);
        pnl2.add(txtDropDist);
        pnl2.add(txtDropTime);
        pnl2.add(txtDropAlti);
        
        pnlCenter.add(pnl3);
        pnl3.setLayout(grObjCenter3);

        pnl3.add(btnCaliberate);
        pnl3.add(btnStart);
        pnl3.add(btnCalcDrop);
        pnl3.add(btnRetract);
        pnl3.add(btnDrop2); 
        pnl3.add(btnDrop4); 

        btnCaliberate.addActionListener(this);
        btnStart.addActionListener(this);
        btnCalcDrop.addActionListener(this);
        btnDrop2.addActionListener(this);
        btnDrop4.addActionListener(this);
        btnRetract.addActionListener(this);

        frame.setSize(gc.transFrameW, gc.transFrameH);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // TODO code application logic here
        Trans2 t = new Trans2("s");

        portList = CommPortIdentifier.getPortIdentifiers();
        System.out.println("here/" + portList.hasMoreElements());

        while (portList.hasMoreElements()) {

            portId = (CommPortIdentifier) portList.nextElement();
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                if (portId.getName().equals(PORT)) {
                    System.out.println("calling constructor...");
                    Trans2 reader = new Trans2();
                }
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            //send signal to drop the package

            try {
                // TODO add your handling code here:
                outputStream = Trans2.serialPort.getOutputStream();
            } catch (IOException ex) {
                Logger.getLogger(Trans2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}