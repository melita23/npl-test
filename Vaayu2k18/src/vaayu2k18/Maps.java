/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vaayu2k18;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Maps {

    static double coordLat = 19.0817645;
    static double coordLng = 72.853231;
    static BufferedWriter bw = null;
    static FileWriter fw = null;

    static String value = null;
    /**
     * @param args the command line arguments
     */
    private static String FILENAME;

    Maps(String filename) throws IOException {
        System.out.println("Entered into Transmit!!");
        
        FILENAME = filename;
    }

    void setMap(double coordLat, double coordLng) throws IOException {

        //bw.append(value);
        fw = new FileWriter(FILENAME);
        bw = new BufferedWriter(fw);

        value = "\n\n\n\n<c>\n<cordlat>" + coordLat + "</cordlat>\n<cordlng>" + coordLng + "</cordlng>\n</c>\n";
        //coordLat = coordLat + 0.0005;
        //coordLng = coordLng + 0.0005;

        try {
            bw.append(value);

            if (bw != null) {
                bw.close();
            }

            if (fw != null) {
                fw.close();
            }
            System.out.println(coordLat);
        } catch (Exception ex) {
            Logger.getLogger(Maps.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}