/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vaayu2k18;

/**
 *
 * @author Others
 */
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.SimpleDateFormat;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.Insets;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class TestFlight extends javax.swing.JFrame implements Runnable, SerialPortEventListener, KeyListener, ActionListener {

    public static final byte DATA_TO_SEND = 'a';
    static CommPortIdentifier portId;
    static Enumeration portList;
    public static final String PORT = "COM4";  //CHECK PORT
    //InputStream inputStream;
    BufferedReader inputStream;
    public static OutputStream outputStream;
    public static SerialPort serialPort;
    Thread readThread;

    private static double yaw, pitch, roll, alti;
    private static double yaw_sum = 0, pitch_sum = 0, roll_sum = 0, alti_sum = 0;
    private static double YAW_OFFSET, ROLL_OFFSET, PITCH_OFFSET, ALTI_OFFSET;
    private int count = 0, or_count = 10, avg_yaw, avg_pitch, avg_roll;
    static boolean offsetBoolean = false;

    //GUI
    JFrame frame;
    JLabel lblTime, lblYaw, lblPitch, lblRoll, lblAltitude;
    static JTextField txtTime, txtYaw, txtPitch, txtRoll, txtAltitude;
    JButton btnCaliberate;
    BorderLayout brObj;
    GridLayout grObjCenter1, grObjSouth;
    JPanel pnlCenter, pnlTime, pnlYaw, pnlPitch, pnlRoll, pnlAltitude;

    //Gauges
    static Gauges g;
    final JFXPanel fxPanelGauge = new JFXPanel();

    //File
    File file1 = new File("All Values.txt");

    //FileReader in = null;
    FileWriter out = null;
    Date date = new Date();
    Date dropDate;
    boolean calc = false;
    PrintWriter pw;

    public TestFlight(String s) {
        try {
            pw = new PrintWriter("C:\\Users\\Others\\Desktop\\Vaayu\\Try.csv");
            pw.append("Time"+','+"Yaw"+','+"Pitch"+','+"Roll"+','+"Altitude"+'\n');
        } catch (IOException ex) {
            Logger.getLogger(Transmission.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("Function: Vaayu2k18 Constructor with String ");
        initJFrameComponents();
        locateTransmissionValues();
    }

    public TestFlight() {
        System.out.println("Function: Vaayu2k18 Constructor  ");
        try {
            serialPort = (SerialPort) portId.open("SimpleReadApp", 2000);
        } catch (PortInUseException e) {
            System.out.println(e);
        }

        try {
            inputStream = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            serialPort.addEventListener(this);
        } catch (TooManyListenersException e) {
            System.out.println(e);
        }

        serialPort.notifyOnDataAvailable(true);

        try {
            serialPort.setSerialPortParams(9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
        } catch (UnsupportedCommOperationException e) {
            System.out.println(e);
        }

        //KeyEvent code:
        this.setFocusable(true);
        System.out.println(this.isFocusable());
        this.addKeyListener(this); // WOAH!!!!! ?!?!
        readThread = new Thread(this);

        readThread.start();

        try {
            System.out.println("Something..");
            outputStream = Transmission.serialPort.getOutputStream();
        } catch (IOException ex) {
            Logger.getLogger(Transmission.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void run() {
        System.out.println("Function: run with the thread ");
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }

    @Override
    public void serialEvent(SerialPortEvent event) {
        switch (event.getEventType()) {
            case SerialPortEvent.BI:
            case SerialPortEvent.OE:
            case SerialPortEvent.FE:
            case SerialPortEvent.PE:
            case SerialPortEvent.CD:
            case SerialPortEvent.CTS:
            case SerialPortEvent.DSR:
            case SerialPortEvent.RI:
            case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                break;
            case SerialPortEvent.DATA_AVAILABLE:

                try {
                    String inputLine = inputStream.readLine();
                    String arrData[] = inputLine.split("#");
                    System.out.println("\nFunction: serialEvent: ");
                    System.out.print(inputLine);

                    try {
                        yaw = Double.parseDouble(arrData[0]);
                    } catch (NumberFormatException nfe) {
                        yaw = 90.00;
                    }

                    try {
                        pitch = Double.parseDouble(arrData[1]);
                    } catch (NumberFormatException nfe) {
                        pitch = 90.00;
                    }

                    try {
                        roll = Double.parseDouble(arrData[2]);
                    } catch (NumberFormatException nfe) {
                        roll = 90.00;
                    }

                    alti = Double.parseDouble(arrData[3]);

                    if (offsetBoolean == false) {
                        txtYaw.setText(Double.toString(yaw));
                        txtPitch.setText(Double.toString(pitch));
                        txtRoll.setText(Double.toString(roll));
                        txtAltitude.setText(Double.toString(alti));
                    } else {
                        count++;

                        if (count <= 10) {
                            YAW_OFFSET += yaw;
                            ROLL_OFFSET += roll;
                            PITCH_OFFSET += pitch;
                            ALTI_OFFSET += alti;

                            txtYaw.setText(Double.toString(yaw));
                            txtPitch.setText(Double.toString(pitch));
                            txtRoll.setText(Double.toString(roll));
                            txtAltitude.setText(Double.toString(alti));
                        } else {
                            if (count == 11) {
                                YAW_OFFSET /= 10;
                                ROLL_OFFSET /= 10;
                                PITCH_OFFSET /= 10;
                                ALTI_OFFSET /= 10;

                                date = new Date();
                                System.out.println("Date: " + date);
                            }

                            txtYaw.setText(Double.toString(yaw - YAW_OFFSET));
                            txtPitch.setText(Double.toString(pitch - PITCH_OFFSET));
                            txtRoll.setText(Double.toString(roll - ROLL_OFFSET));
                            txtAltitude.setText(Double.toString(alti - ALTI_OFFSET));
                            
                            Date newdate = new Date();
                            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

                            System.out.println("NDate: " + newdate);

                            long diff = newdate.getTime() - date.getTime();

                            long diffMSeconds = diff % 1000;
                            long diffSeconds = diff / 1000 % 60;
                            long diffMinutes = diff / (60 * 1000) % 60;
                            long diffHours = diff / (60 * 60 * 1000) % 24;

                            String strDate = diffHours + ":" + diffMinutes + ":" + diffSeconds + ":" + diffMSeconds;

                            txtTime.setText(strDate);

                            pw.append(strDate+','+Double.toString(yaw - YAW_OFFSET)+','+Double.toString(pitch - PITCH_OFFSET)+','+Double.toString(roll - ROLL_OFFSET)+','+Double.toString(alti - ALTI_OFFSET)+'\n');
                        }
                    }
                } catch (IOException e) {
                    System.out.println("\n\n" + e + "\n");
                }

                break;
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(btnCaliberate)) {
            System.out.println("\n\nCaliberate");
            if (offsetBoolean == false) //SET OFFSET VALUES
            {
                System.out.println("Caliberate In");
                offsetBoolean = true;
            }
            System.out.println();
        }
    }

    void initJFrameComponents() {
        frame = new JFrame("GUI!!");

        lblTime = new JLabel("  Time  ");
        lblYaw = new JLabel("    Yaw    ");
        lblPitch = new JLabel("    Pitch    ");
        lblRoll = new JLabel("    Roll    ");
        lblAltitude = new JLabel("Altitude");
        
        txtTime = new JTextField(6);
        txtYaw = new JTextField(6);
        txtPitch = new JTextField(6);
        txtRoll = new JTextField(6);
        txtAltitude = new JTextField(6);
        
        btnCaliberate = new JButton("Caliberate");
        
        lblTime.setFont(new java.awt.Font("Tahoma", 1, 40));
        lblYaw.setFont(new java.awt.Font("Tahoma", 1, 40));
        lblPitch.setFont(new java.awt.Font("Tahoma", 1, 40));
        lblRoll.setFont(new java.awt.Font("Tahoma", 1, 40));
        lblAltitude.setFont(new java.awt.Font("Tahoma", 1, 40));
        
        txtTime.setHorizontalAlignment(JTextField.CENTER);
        txtTime.setFont(new java.awt.Font("Times New Roman", 1, 45));
        txtTime.setEditable(false);
        txtYaw.setHorizontalAlignment(JTextField.CENTER);
        txtYaw.setFont(new java.awt.Font("Times New Roman", 1, 45));
        txtYaw.setEditable(false);
        txtPitch.setHorizontalAlignment(JTextField.CENTER);
        txtPitch.setFont(new java.awt.Font("Times New Roman", 1, 45));
        txtPitch.setEditable(false);
        txtRoll.setHorizontalAlignment(JTextField.CENTER);
        txtRoll.setFont(new java.awt.Font("Times New Roman", 1, 45));
        txtRoll.setEditable(false);
        txtAltitude.setHorizontalAlignment(JTextField.CENTER);
        txtAltitude.setFont(new java.awt.Font("Times New Roman", 1, 45));
        txtAltitude.setEditable(false);
        
        btnCaliberate.setFont(new java.awt.Font("Tahoma", 1, 45));
        
        brObj = new BorderLayout();
        grObjCenter1 = new GridLayout(3, 3, 10, 10);
        pnlCenter = new JPanel();
        pnlTime = new JPanel();
        pnlYaw = new JPanel();
        pnlPitch = new JPanel();
        pnlRoll = new JPanel();
        pnlAltitude = new JPanel();
    }

    void locateTransmissionValues() {
        frame.setLayout(brObj);

        frame.add(pnlCenter, BorderLayout.CENTER);

        pnlCenter.setLayout(grObjCenter1);

        pnlCenter.add(pnlYaw);              //1,2
        pnlYaw.add(lblYaw);
        pnlYaw.add(txtYaw);
        pnlCenter.add(pnlPitch);            //1,3
        pnlPitch.add(lblPitch);
        pnlPitch.add(txtPitch);
        pnlCenter.add(pnlRoll);             //1,4
        pnlRoll.add(lblRoll);
        pnlRoll.add(txtRoll);
        pnlCenter.add(pnlAltitude);         //2,1
        pnlAltitude.add(lblAltitude);
        pnlAltitude.add(txtAltitude);
        pnlCenter.add(pnlTime);             //1,1
        pnlTime.add(lblTime);
        pnlTime.add(txtTime);
        pnlCenter.add(btnCaliberate);       //4,1
        
        btnCaliberate.addActionListener(this);
        
        frame.setBounds(450, 200, 700, 700);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // TODO code application logic here
        TestFlight t = new TestFlight("s");

        portList = CommPortIdentifier.getPortIdentifiers();
        System.out.println("here/" + portList.hasMoreElements());

        while (portList.hasMoreElements()) {

            portId = (CommPortIdentifier) portList.nextElement();
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                if (portId.getName().equals(PORT)) {
                    System.out.println("calling constructor...");
                    Transmission reader = new Transmission();
                }
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            //send signal to drop the package

            try {
                // TODO add your handling code here:
                outputStream = Transmission.serialPort.getOutputStream();
            } catch (IOException ex) {
                Logger.getLogger(Transmission.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}