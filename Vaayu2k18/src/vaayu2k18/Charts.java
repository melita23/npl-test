/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vaayu2k18;

/**
 *
 * @author Others
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.io.*;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Timer;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

class Datas {

    static ArrayList<Double> data = new ArrayList<Double>();
    static int i = 0;
    double teee = 0.0;
    GlobalConfig gc;

    Datas(GlobalConfig gc) {
        teee = 0.0;
        this.gc = gc;
    }

    public void readd() throws IOException {
        FileReader in = null;
        FileWriter out = null;

        try {
            in = new FileReader(gc.alti);

            double t = 0;
            String s = "";
            int c;
            while ((c = in.read()) != -1) {

                if (c == ' ') {
                    t = parseDouble(s);
                    teee = t;
                    s = "";
                } else {
                    s += (char) c;
                }
            }
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }

    }

    double getData() {
        return teee;
    }
}

public class Charts extends ApplicationFrame implements ActionListener {

    /**
     * The time series data.
     */
    private TimeSeries series;

    /**
     * The most recent value added.
     */
    private double lastValue = 100.0;

    /**
     * Timer to refresh graph after every 1/4th of a second
     */
    private Timer timer = new Timer(250, this);

    static GlobalConfig gc;

    /**
     * Constructs a new dynamic chart application.
     *
     * @param title the frame title.
     */
    public Charts(final String title) {

        super(title);
        this.series = new TimeSeries("Altitude Data", Millisecond.class);
        
        gc = new GlobalConfig();

        final TimeSeriesCollection dataset = new TimeSeriesCollection(this.series);
        final JFreeChart chart = createChart(dataset);

        timer.setInitialDelay(1000);

        //Sets background color of chart
        chart.setBackgroundPaint(Color.LIGHT_GRAY);

        //Created JPanel to show graph on screen
        final JPanel content = new JPanel(new BorderLayout());

        //Created Chartpanel for chart area
        final ChartPanel chartPanel = new ChartPanel(chart);

        //Added chartpanel to main panel
        content.add(chartPanel);

        //Sets the size of whole window (JPanel)
        chartPanel.setPreferredSize(new java.awt.Dimension(gc.altiFrameW, gc.altiFrameH));

        //Puts the whole content on a Frame
        setContentPane(content);

        timer.start();

    }

    private JFreeChart createChart(final XYDataset dataset) {
        final JFreeChart result = ChartFactory.createTimeSeriesChart(
                "",
                "Time (HH:MM:SS)",
                "Altitude (ft)",
                dataset,
                true,
                true,
                false
        );

        final XYPlot plot = result.getXYPlot();

        plot.setBackgroundPaint(new Color(0xffffe0));
        plot.setDomainGridlinesVisible(true);
        plot.setDomainGridlinePaint(Color.lightGray);
        plot.setRangeGridlinesVisible(true);
        plot.setRangeGridlinePaint(Color.lightGray);

        ValueAxis xaxis = plot.getDomainAxis();
        xaxis.setAutoRange(true);

        //Domain axis would show data of 60 seconds for a time
        xaxis.setFixedAutoRange(60000.0);  // 60 seconds
        xaxis.setVerticalTickLabels(true);

        ValueAxis yaxis = plot.getRangeAxis();
        yaxis.setRange(0.0, 200.0);

        return result;
    }

    public void actionPerformed(final ActionEvent e) {
        Datas d = new Datas(gc);
        try {
            d.readd();
        } catch (IOException ex) {
            Logger.getLogger(Charts.class.getName()).log(Level.SEVERE, null, ex);
        }

        double te;
        te = d.getData();
        final double factor = 0.9 + 0.2 * Math.random();
        this.lastValue = this.lastValue * factor;

        final Millisecond now = new Millisecond();
        this.series.add(new Millisecond(), te);

        //System.out.println("Current Time in Milliseconds = " + now.toString() + ", Current Value : " + te);
    }
    
    void setLoc(Charts demo) {
        demo.setLocation(gc.altiFrameX, gc.altiFrameY);
    }

    public static void main(final String[] args) throws IOException {
        
        final Charts demo = new Charts("Altitude Chart");
        demo.setLoc(demo);
        demo.pack();
        demo.setVisible(true);
	//hello
    }
}
