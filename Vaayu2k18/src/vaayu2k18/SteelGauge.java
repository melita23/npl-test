/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vaayu2k18;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import eu.hansolo.steelseries.gauges.*;
import eu.hansolo.steelseries.extras.AirCompass;
import java.awt.GridLayout;

public class SteelGauge {
    static Radial gVelocity = new Radial();
    static DisplayCircular gBattery = new DisplayCircular();
    static Radial1Vertical gVario = new Radial1Vertical();
    static AirCompass gHeading = new AirCompass();
    static int frameX, frameY, frameW, frameH;
   
    SteelGauge(GlobalConfig gc) {
        frameX = gc.gaugesFrameX;
        frameY = gc.gaugesFrameY;
        frameW = gc.gaugesFrameW;
        frameH = gc.gaugesFrameH;
        
        gVelocity.setTitle("Velocity");
        gVelocity.setUnitString("Kmph");
        gBattery.setTitle("Battery");
        gBattery.setUnitString("Volts");
        gVario.setTitle("Variometer");
        gVario.setUnitString("ft/s");
        gVario.setMinValue(-30);
        gVario.setMaxValue(30);
        
        
        //steelseries.TickLabelOrientation.HORIZONTAL,
    }
    
    public static void createAndShowUI() {
        JFrame frame = new JFrame();
        //JPanel panel = new JPanel();
        GridLayout gr = new GridLayout(2,2,0,0);
        
        frame.setLayout(gr);
        frame.add(gVelocity);
        frame.add(gBattery);
        frame.add(gVario);
        frame.add(gHeading);
        
        frame.setLocation(frameX, frameY);
        frame.setSize(frameW, frameH);
        //frame.pack();
        frame.setVisible(true);
        gVelocity.setSize(500,500);
    }
    
    public static void setGauges(double velocity, double battery, double vario, double heading){
        gVelocity.setValue(velocity);
        gBattery.setValue(battery);
        gVario.setValue(vario);
        gHeading.setValue(heading);
    }
}