/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vaayu2k18;

/**
 *
 * @author Others
 */
import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.Gauge.KnobType;
import eu.hansolo.medusa.Gauge.LedType;
import eu.hansolo.medusa.Gauge.NeedleShape;
import eu.hansolo.medusa.Gauge.NeedleSize;
import eu.hansolo.medusa.Gauge.NeedleType;
import eu.hansolo.medusa.Gauge.ScaleDirection;
import eu.hansolo.medusa.Gauge.SkinType;
import eu.hansolo.medusa.GaugeBuilder;
import eu.hansolo.medusa.LcdDesign;
import eu.hansolo.medusa.LcdFont;
import eu.hansolo.medusa.Section;
import eu.hansolo.medusa.TickLabelLocation;
import eu.hansolo.medusa.TickLabelOrientation;
import eu.hansolo.medusa.TickMarkType;
import eu.hansolo.medusa.skins.BulletChartSkin;
import eu.hansolo.medusa.skins.ModernSkin;
import eu.hansolo.medusa.skins.SpaceXSkin;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.Random;
import javafx.beans.property.DoubleProperty;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Others
 */
public class Gauges {
    
    private static Random RND = new Random();
    private static double MIN_CELL_SIZE;
    private static double PREF_CELL_SIZE;
    private static double MAX_CELL_SIZE;
    private static int noOfNodes, hGap, vGap;
    private static Gauge gaugeStall, gaugeAngle, gaugeVelocity, gaugeBattery, gaugeHeading, gauge;
    static JFrame frame;
    final JFXPanel fxPanelGauge = new JFXPanel();
    static JPanel panel;
    
    Gauges(GlobalConfig gc) {
        MIN_CELL_SIZE  = gc.gaugeMin;
        PREF_CELL_SIZE = gc.gaugePref;
        MAX_CELL_SIZE  = gc.gaugeMax;
        noOfNodes      = 0;
        hGap           = gc.gaugeHGap;
        vGap           = gc.gaugeVGap;
        
        frame = new JFrame();
        panel = new JPanel();
    }
    
    void initGauges()
    {
        gaugeVelocity = GaugeBuilder.create()
                         .skinType(Gauge.SkinType.MODERN)
                         .sections(new Section(85, 90, "", Color.rgb(204, 0, 0, 0.5)),
                                   new Section(90, 95, "", Color.rgb(204, 0, 0, 0.75)),
                                   new Section(95, 100, "", Color.rgb(204, 0, 0)))
                         .title("Velocity")
                         .unit("kmph")
                         .threshold(85)
                         .thresholdVisible(true)
                         .animated(true)
                         .build();
        
        gaugeBattery = GaugeBuilder.create()
                         .skinType(Gauge.SkinType.FLAT)
                         .barColor(Color.CYAN)
                         .backgroundPaint(Color.WHITE)
                         .minValue(0)
                         .maxValue(15)
                         .title("Battery")
                         .unit("Volts")
                         .build();
        
        gaugeStall = GaugeBuilder.create()
                                  .skinType(Gauge.SkinType.TINY)
                                  .animated(true)
                                  .minValue(0)
                                  .title("Stall Angle")
                                  .titleColor(Color.BLUE)
                                  .unit("abc")
                                  .sections(new Section(0, 33, Color.rgb(0, 200, 0, 0.75)),
                                            new Section(33, 66, Color.rgb(200, 200, 0, 0.75)),
                                            new Section(66, 100, Color.rgb(200, 0, 0, 0.75)))
                                  .backgroundPaint(Color.WHITE)
                                  .build();

        gaugeHeading = GaugeBuilder.create()
                          .minValue(0)
                          .maxValue(359)
                          .startAngle(180)
                          .angleRange(360)
                          .autoScale(false)
                          .customTickLabelsEnabled(true)
                          .customTickLabels("N", "", "", "", "", "", "", "", "",
                                            "E", "", "", "", "", "", "", "", "",
                                            "S", "", "", "", "", "", "", "", "",
                                            "W", "", "", "", "", "", "", "", "")
                          .customTickLabelFontSize(72)
                          .minorTickMarksVisible(false)
                          .mediumTickMarksVisible(false)
                          .majorTickMarksVisible(false)
                          .valueVisible(false)
                          .needleType(NeedleType.FAT)
                          .needleShape(NeedleShape.FLAT)
                          .knobType(KnobType.FLAT)
                          .knobColor(Gauge.DARK_COLOR)
                          .borderPaint(Gauge.DARK_COLOR)
                          .backgroundPaint(Color.WHITE)
                          .build();
        
        gaugeAngle = GaugeBuilder.create()
                          //.scaleDirection(ScaleDirection.COUNTER_CLOCKWISE)
                          .tickLabelLocation(TickLabelLocation.OUTSIDE)
                          .animated(true)
                          .startAngle(-180)
                          .angleRange(180)
                          .minValue(-30)
                          .maxValue(30)
                          .zeroColor(Color.ORANGE)
                          .title("Variometer")  
                          .unit("ft/s")
                          .customFont(Font.font("Tahoma", 20))
                          .majorTickMarkType(TickMarkType.TRIANGLE)
                          .backgroundPaint(Color.WHITE)
                          .build();
        
        gauge = GaugeBuilder.create()
                .skinType(Gauge.SkinType.SIMPLE)
                .sections(new Section(0, 16.66666, "0", Color.web("#11632f")),
                                    new Section(16.66666, 33.33333, "1", Color.web("#36843d")),
                                    new Section(33.33333, 50.0, "2", Color.web("#67a328")),
                                    new Section(50.0, 66.66666, "3", Color.web("#80b940")),
                                    new Section(66.66666, 83.33333, "4", Color.web("#95c262")),
                                    new Section(83.33333, 100.0, "5", Color.web("#badf8d")))
                          .title("Simple")
                          .threshold(50)
                          .title("Extra")
                          .unit("")
                          .animated(true)
                          .build();
    }
    
    void locateGauges() 
    {
        
        try
        {
            System.out.println("DONE start");

            GridPane pane = new GridPane();
            pane.setHalignment(pane, HPos.CENTER);
            pane.setStyle("-fx-background-color: #C0C0C0;");
            pane.setHgap(hGap);
            pane.setVgap(vGap);

            //pane.add(gauge, 0, 0);
            /*pane.add(gaugeVelocity, 1, 1);
            pane.add(gaugeBattery, 1, 0);
            //pane.add(gaugeStall, 0, 1);
            pane.add(gaugeHeading, 2, 0);
            pane.add(gaugeAngle, 0, 0);*/
            
            pane.add(gaugeVelocity, 0, 0);
            pane.add(gaugeBattery, 1, 0);
            //pane.add(gaugeStall, 0, 1);
            pane.add(gaugeHeading, 0, 1);
            pane.add(gaugeAngle, 1, 1);
            
            for (int i = 0 ; i < 2 ; i++) {
                pane.getColumnConstraints().add(new ColumnConstraints(MIN_CELL_SIZE, PREF_CELL_SIZE, MAX_CELL_SIZE));
            }
            for (int i = 0 ; i < 2 ; i++) {
                pane.getRowConstraints().add(new RowConstraints(MIN_CELL_SIZE, PREF_CELL_SIZE, MAX_CELL_SIZE));
            }
            pane.setBackground(new Background(new BackgroundFill(Color.rgb(90, 90, 90), CornerRadii.EMPTY, Insets.EMPTY)));

            Scene scene = new Scene(pane);
            calcNoOfNodes(pane);
            fxPanelGauge.setScene(scene);
            
            frame.add(fxPanelGauge);
            //panel.setLayout(new BorderLayout());
            //panel(scene);
            frame.setSize(200,200);
            frame.setVisible(true);
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    private static void calcNoOfNodes(Node node)
    {
        if (node instanceof Parent)
        {
            if (((Parent) node).getChildrenUnmodifiable().size() != 0)
            {
                ObservableList<Node> tempChildren = ((Parent) node).getChildrenUnmodifiable();
                noOfNodes += tempChildren.size();
                for (Node n : tempChildren) { calcNoOfNodes(n); }
            }
        }
    }
    
    void setGauge(double velocity, double battery, double heading, double vario)
    {
        //gauge.setValue(0);
        
        gaugeVelocity.setValue(velocity);
        gaugeBattery.setValue(battery);
        gaugeHeading.setValue(heading);
        gaugeAngle.setValue(vario);
        
        /*double var = 0;
        
        if(aoa<-5 || aoa>=14.5)
            var = 100;
        else if(aoa>=0) {
            if(aoa<=11)
                var = aoa*(33.33/11);
            else if(aoa<=13.5)
                var = 33.33+((aoa-11)*(33.33/2.5));
            else
                var = 66.66+((aoa-13.5)*33.33);
        } else {
            aoa = -aoa;
            
            if(aoa<=2)
                var = aoa*(33.33/2);
            else if(aoa<=3)
                var = 33.33+((aoa-2)*33.33);
            else
                var = 66.66+((aoa-3)*33.33);
        }
        
        gaugeStall.setValue(var);*/
    }
}